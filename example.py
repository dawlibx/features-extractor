# Lib for time measurement
import timeit
from features_extractor import *

EXAMPLE_URL = 'https://www.youtube.com/watch?v=GtfZbj4J71A'

# Example process with time measurement
execution_time = timeit.timeit(lambda: FeaturesExtractor().execute(EXAMPLE_URL), number = 1)
print 'Execution time: ' + str(execution_time) + ' [s]'
