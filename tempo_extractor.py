import librosa
from IPython import embed

class TempoExtractor:
  # int > 0 Number of audio samples between successive onset measurements
  HOP_LENGTH = 512

  def execute(self, file_path):
    # audio_time_series : np.ndarray [shape=(n,), None] Audio time series
    # sample_rate : number > 0 [scalar] Sampling rate of audio_time_series
    audio_time_series, sample_rate = librosa.load(file_path)
    # onset_envelope : np.ndarray [shape=(n,), None] Optional pre-computed onset strength envelope
    onset_envelope = librosa.onset.onset_strength(
      y = audio_time_series, sr = sample_rate, hop_length = self.HOP_LENGTH
    )
    # Compute the tempogram, which is a local autocorrelation of the onset strength envelope
    tempogram = librosa.feature.tempogram(
      onset_envelope = onset_envelope, sr = sample_rate, hop_length = self.HOP_LENGTH
    )
    # Compute global onset autocorrelation
    global_autocorrelation = librosa.autocorrelate(onset_envelope, max_size = tempogram.shape[0])
    # Normalize global onset autocorrelation
    global_autocorrelation = librosa.util.normalize(global_autocorrelation)
    # Estimate the global tempo of soundtrack
    tempo = librosa.beat.estimate_tempo(
      onset_envelope, sr = sample_rate, hop_length = self.HOP_LENGTH
    )

    return tempo
