class Presenter:
  LINE_SEPARATOR = '\n' + 50 * '-'

  def present_tempo(self, tempo):
    print '\nVideo\'s music tempo: ' + str(tempo) + ' [BPM]' + self.LINE_SEPARATOR
