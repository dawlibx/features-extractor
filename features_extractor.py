from tempo_extractor import *
from downloader import *
from presenter import *

class FeaturesExtractor:
  # Main process
  # Returns [Array<String>]
  def execute(self, url):
    # Download chosen video from YouTube
    file_path = Downloader().execute(url)
    # Extract BPM of a given track
    tempo = TempoExtractor().execute(file_path)

    self.__present_results(tempo)

    return [tempo]

  # Present results
  def __present_results(self, tempo):
    presenter = Presenter()

    presenter.present_tempo(tempo)


