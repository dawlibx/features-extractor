import pafy
import os.path

class Downloader:
  # Directory where videos are downloaded to
  VIDEOS_DIR_NAME = './videos/'
  # Prefered format of videos
  PREFERED_FORMAT = 'mp4'

  # Usage example:
  #   Downloader().execute('https://www.youtube.com/watch?v=GtfZbj4J71A')
  # Returns [String] path to the downloaded video file
  def execute(self, url):
    # using a YouTube url create a video instance containing basic informations about video
    video = pafy.new(url)
    # video settings - get the best resolution for a chosen file format (mp4, webm, flv or 3gp):
    setup = video.getbest(self.PREFERED_FORMAT)
    file_path = self.__get_file_path(video, setup)
    # download video from YouTube if it does not exist locally - it can take a while
    if not self.__video_exists(file_path): setup.download(file_path)

    return file_path

  # Prepares file name with chosen extension
  # Returns [String] path to the file
  def __get_file_path(self, video, setup):
    file_name = (video.title + "." + setup.extension).lower().replace (" ", "_")

    return self.VIDEOS_DIR_NAME + file_name

  # Checks if given file is already downloaded
  # Returns [Boolean] if file is already downloaded it returns True, in the other case it returns False
  def __video_exists(self, file_path):
    return os.path.isfile(file_path)
